//
//  ViewController.swift
//  FlickFinder
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoTitleLabel: UILabel!
    @IBOutlet weak var defaultLabel: UILabel!
    @IBOutlet weak var phraseTextField: UITextField!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    
    let BASE_URL = "https://api.flickr.com/services/rest/"
    let METHOD_NAME = "flickr.photos.search"
    let API_KEY = "f3bb2eac14a4199ccaeb72bc876e374d"
    let EXTRAS = "url_m"
    let SAFE_SEARCH = "1"
    let DATA_FORMAT = "json"
    let NO_JSON_CALLBACK = "1"
    let BOUNDING_BOX_HALF_WIDTH = 1.0
    let BOUNDING_BOX_HALF_HEIGHT = 1.0
    let LAT_MIN = -90
    let LAT_MAX = 90
    let LONG_MIN = -180
    let LONG_MAX = 180

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var tapRecognizer: UITapGestureRecognizer? = nil
    
    @IBAction func searchPhotosByPhraseButton(sender: AnyObject) {
        if !self.phraseTextField.text.isEmpty {
            let args = [
            "method": METHOD_NAME,
            "api_key": API_KEY,
            "text": self.phraseTextField.text,
            "safe_search": SAFE_SEARCH,
            "extras": EXTRAS,
            "format": DATA_FORMAT,
            "nojsoncallback": NO_JSON_CALLBACK
            ]
            self.latitudeTextField.text = ""
            self.longitudeTextField.text = ""
            getImageFromFlickrBySearch(args)
        }
        else {
            self.photoTitleLabel.text = "Enter a phrase to search."
        }
    }

    @IBAction func searchPhotosByLatLonButton(sender: AnyObject) {
        if !self.latitudeTextField.text.isEmpty && !self.longitudeTextField.text.isEmpty {
            if isvalidLatitude() && isvalidLongitude() {
                let args = [
                    "method": METHOD_NAME,
                    "api_key": API_KEY,
                    "bbox": createBoundingBoxString(),
                    "safe_search": SAFE_SEARCH,
                    "extras": EXTRAS,
                    "format": DATA_FORMAT,
                    "nojsoncallback": NO_JSON_CALLBACK
                ]
                self.phraseTextField.text = ""
                getImageFromFlickrBySearch(args)
            }
            else {
                var message: String = ""
                if (!isvalidLatitude()) {
                    message += "Latitude is invalid.\n Lat should be between -90 to 90."
                }
                if (!isvalidLongitude()) {
                    message += "Longitude is invalid.\n Long should be betwen -180 to 180."
                }
                self.photoTitleLabel.text = message
            }
        }
        else {
            self.photoTitleLabel.text = "Both Lat and Long should be entered."
        }
    }
    
    func isvalidLatitude() -> Bool {
        if let latitude: Int? = self.latitudeTextField.text.toInt() {
            return latitude <= LAT_MAX && latitude >= LAT_MIN
        }
        return false
    }
    
    func isvalidLongitude() -> Bool {
        if let longitude: Int? = self.longitudeTextField.text.toInt() {
            return longitude <= LONG_MAX && longitude >= LONG_MIN
        }
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()                     // note colon used at end of action string;
        tapRecognizer = UITapGestureRecognizer(target: self, action: "handleSingleTap:")
        tapRecognizer?.numberOfTapsRequired = 1
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.view.addGestureRecognizer(tapRecognizer!)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.removeGestureRecognizer(tapRecognizer!)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    }
    
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if self.photoImageView.image != nil {
            self.defaultLabel.alpha = 0.0
        }
        self.bottomConstraint.constant -= self.getKeyboardHeight(notification)
        // self.view.frame.origin.y -= self.getKeyboardHeight(notification)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if self.photoImageView.image != nil {
            self.defaultLabel.alpha = 1.0
        }
        self.bottomConstraint.constant += self.getKeyboardHeight(notification)
        // self.view.frame.origin.y += self.getKeyboardHeight(notification)
    }
    
    func getKeyboardHeight(notification: NSNotification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as NSValue // of CGRect
        return keyboardSize.CGRectValue().height
    }
    
    func createBoundingBoxString() -> String {
        let latitude = (self.latitudeTextField.text as NSString).doubleValue
        let longitude = (self.longitudeTextField.text as NSString).doubleValue
        return "\(longitude - BOUNDING_BOX_HALF_WIDTH),\(latitude - BOUNDING_BOX_HALF_HEIGHT)," +
            "\(longitude + BOUNDING_BOX_HALF_WIDTH),\(latitude + BOUNDING_BOX_HALF_HEIGHT)"
    }
    
    /* Function makes first request to get a random page, then it makes a request to get an image with the random page */
    func getImageFromFlickrBySearch(methodArguments: [String : AnyObject]) {
        
        let session = NSURLSession.sharedSession()
        let urlString = BASE_URL + escapedParameters(methodArguments)
        let url = NSURL(string: urlString)!
        let request = NSURLRequest(URL: url)
        
        let task = session.dataTaskWithRequest(request) {data, response, downloadError in
            if let error = downloadError? {
                println("Could not complete the request \(error)")
            } else {
                
                var parsingError: NSError? = nil
                let parsedResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &parsingError) as NSDictionary
                
                if let photosDictionary = parsedResult.valueForKey("photos") as? [String:AnyObject] {
                    /* 1 - Get random page, then make new request */
                    if let totalPages = photosDictionary["pages"] as? Int {
                        
                        /* Flickr API - will only return up the 4000 images (100 per page * 40 page max) */
                        let pageLimit = min(totalPages, 40)
                        let randomPage = Int(arc4random_uniform(UInt32(pageLimit) + 1))
                        self.getImageFromFlickrByPage(methodArguments, page: randomPage)
                        
                    } else {
                        println("Cant find key 'pages' in \(photosDictionary)")
                    }
                }
            }
        }
        task.resume()
    }
    
    func getImageFromFlickrByPage(args: [String : AnyObject], page: Int) {
        let session = NSURLSession.sharedSession()
        var arguments = args                    // make args an editable variable;
        arguments["page"] = page
        
        let urlString = BASE_URL + escapedParameters(arguments)
        let url = NSURL(string: urlString)!
        let request = NSURLRequest(URL: url)
        
        let task = session.dataTaskWithRequest(request) {data, response, downloadError in
            // below are the method arguments for the completionHandler, which runs on a background thread;
            if let error = downloadError? {
                println("Could not complete the request \(error)")
            }
            else {
                var parsingError: NSError? = nil
                // error is an inout parameter that is passed by reference so it requires '&' in function calls
                let parsedResult = NSJSONSerialization.JSONObjectWithData(
                    data, options: NSJSONReadingOptions.AllowFragments, error: &parsingError) as NSDictionary
                
                if let photosDict = parsedResult.valueForKey("photos") as? [String: AnyObject] {
                    let totalPics = photosDict["total"] != nil ? (photosDict["total"] as NSString).integerValue : 0
                    if totalPics == 0 {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.photoTitleLabel.text = "No Photos Found. Search Again."
                            self.defaultLabel.alpha = 1.0
                            self.photoImageView.image = nil
                        })
                    }
                    else if let photosArray = photosDict["photo"] as? [[String: AnyObject]] {
                        let randomIndex = Int(arc4random_uniform(UInt32(photosArray.count)))
                        let photoInfo = photosArray[randomIndex] as [String: AnyObject]
                        
                        let photoTitle = photoInfo["title"] as? String
                        let imageUrlStr = photoInfo["url_m"] as? String
                        let imageURL = NSURL(string: imageUrlStr!)!
                        
                        if let imageData = NSData(contentsOfURL: imageURL) {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.defaultLabel.alpha = 0.0
                                self.photoImageView.image = UIImage(data: imageData)
                                self.photoTitleLabel.text = "\(photoTitle!)"
                            })
                        }
                        else {
                            println("Image does not exist at \(imageURL)")
                        }
                    }
                    else {
                        println("Can't find key 'photo' in \(photosDict)")
                    }
                }
                else {
                    println("Can't find key 'photos' in \(parsedResult)")
                }
            }
        }
        task.resume()
    }
    
    func escapedParameters(parameters: [String : AnyObject]) -> String {
        var urlVars = [String]()
        
        for (key, value) in parameters {
            // ensure value is a string
            let strValue = "\(value)"
            
            // escape the string
            let escapedVal = strValue.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            // replace spaces with '+'
            let replacedVal = escapedVal.stringByReplacingOccurrencesOfString(" ", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
            
            urlVars += [key + "=" + "\(replacedVal)"]
        }
        return (!urlVars.isEmpty ? "?" : "") + join("&", urlVars)
    }
}

